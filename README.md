# pressure_logger


### Description
This repo contains a simple implementation of a TcpServer delivering values sampled from a dummy sensor.
The sensor provides a pressure value each 5ms while the service recording logs an average of these values each 150ms.



### Dependencies

- cmake 3.5
- catch
- Boost::asio


The Catch header is enclosed to the project. 

The boost library can be imported via Cmake.


### Classes

#### DummySensor

The sensor provides pressure data reading from the file "data.txt" enclosed in the project. Pressure values are encoded as int16_t.

#### Pressure Reader Service 

This service reads the value provided by the sensor then waits for 5ms. It writes an average value of the readings received each 150ms in addition to a timestamp in ms in epoch format into a log file.
A limit is set for the size of the log file. When the log reaches this limit, the service stops logging to avoid disk saturation.

It has been noticed that the mainLoop of the service takes 153ms instead of 150, which will cause the loss of some readings over time. To cope with this, the wait time between readings could be reduced or the calculus of timestamp avoided (and deduced incrementally)

#### TcpServer 

The TcpServer uses the Boost Library. It is synchronous and is largely inspired from https://www.boost.org/doc/libs/1_42_0/doc/html/boost_asio/tutorial/tutdaytime2.html 
This version of the server only supports one connection simultaneously. It exposes an endpoint for log recovery on 0.0.0.0:5000 by default. 
To build and run the server :

```
cd src/TcpServer
mkdir build
cd build
cmake ..
make
./tcp_server
```


When receiving a request from the client, the server sends first the size of the log file then writes the content of the log file by buffers of 1500 bytes (by convention).

#### TcpClient


The TcpClient uses the Boost Library and is largely inspired from https://www.boost.org/doc/libs/1_42_0/doc/html/boost_asio/tutorial/tutdaytime1.html
This version of the client requests the server and simply prints the data it receives. 
To build and run the client :

```
cd src/TcpClient
mkdir build
cd build
cmake ..
make
./tcp_client
```

### Testing

Testing done with the library Catch.
To run tests :

```
cd test
mkdir build
cd build
cmake ..
make
./pressureLoggerTest
```

Expected output :

```
/pressure_logger/test/cmake-build-debug/pressureLoggerTest
===============================================================================
All tests passed (12 assertions in 5 test cases)


Process finished with exit code 0
```

### Production

This code in its current version is not production grade. Some ideas which would help improving the quality of this code but which have not been implemented so far :

- Implementation of a DAO with a sqliteDB to avoid logging directly into a logfile
- To remove hard coded configuration and to use a YAML parser or similar
- To implement TcpPacket class instead of sending only data, including checksum and information regarding the payload
- Implement compression algorithm to reduce the amount of data to be transferred via the TcpServer
- Add an endpoint to retrieve information about the system status
- Add an endpoints to reset the PressureReaderService
- Use an asynchronous Tcp server to handle multiple connections
- Use a log rotation process (either as a file or as a db) instead of stopping the log
 


