#ifndef PRESSURE_LOGGER_TCPSERVER_H
#define PRESSURE_LOGGER_TCPSERVER_H
#include <iostream>
#include <string>
#include <boost/asio.hpp>
using boost::asio::ip::tcp;

#endif //PRESSURE_LOGGER_TCPSERVER_H

class TcpServer {

public:
    TcpServer();
    ~TcpServer();

protected:
    const int16_t bufferSize = 1500; // as a convention
    bool shouldStop;
    std::string dataLogName = "pressureLog.txt";
    char * buffer; // local buffer to return data to the client
    int serverPort = 5000;

public:
    int getServerPort() const;
    void mainLoop();
    std::ifstream readerInit();
    int getLogSize();
    std::string readerGetValue(std::ifstream *_dataStream, int dataLength);

    void setLogFileName(const char string[21]);

    void setLogFileName(std::string newFilename);
};