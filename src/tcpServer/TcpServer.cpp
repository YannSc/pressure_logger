#include <fstream>
#include "TcpServer.h"

TcpServer::TcpServer() {
    shouldStop = false;
    buffer =  new char[bufferSize];;
}

TcpServer::~TcpServer() {
    if (buffer != NULL) {
        delete buffer;
    }
}

void TcpServer::mainLoop() {
    try {
        boost::asio::io_service io_service;
        tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), serverPort));

        while (!shouldStop) {
            buffer = new char[bufferSize];
            tcp::socket socket(io_service);
            acceptor.accept(socket);
            std::cout << "Client request received ... ";
            std::ifstream  dataStream;
            dataStream = readerInit();

            int fileSize = getLogSize();
            boost::system::error_code ignored_error;
            std::string fileSizeAsString = std::to_string(fileSize).append("\n");
            boost::asio::write(socket, boost::asio::buffer(fileSizeAsString),
                               boost::asio::transfer_all(), ignored_error);
            std::cout << "Current LogSize : " << fileSize << "\n";

            int data_length = 0;
            while(data_length < fileSize){
                std::string data = readerGetValue(&dataStream, data_length);
                boost::asio::write(socket, boost::asio::buffer(data),
                                   boost::asio::transfer_all(), ignored_error);
                data_length += bufferSize;
                std::cout << "Total bytes sent : " << data_length << "\n";
            }

            if (buffer != NULL) {
                delete buffer;
                buffer = NULL;
            }

        }
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

std::ifstream TcpServer::readerInit() {
    std::ifstream dataStream;
    dataStream.open(dataLogName);
    return dataStream;
}

int32_t TcpServer::getLogSize(){
    std::ifstream dataStream;
    int32_t logLength = 0;
    dataStream.open(dataLogName, std::ios::binary | std::ios::ate);;
    logLength = dataStream.tellg();
    return logLength;
}

std::string TcpServer::readerGetValue(std::ifstream * _dataStream, int32_t dataLength) {
    int32_t logLength = getLogSize();
    std::ifstream * dataStream = _dataStream;

    if (logLength < dataLength) {
        dataStream->read(buffer, logLength);
    }
    else if ((logLength - dataLength) > bufferSize) {
        dataStream->read(buffer, bufferSize);
    } else {
        buffer = new char[bufferSize];
        dataStream->read(buffer, logLength - dataLength);
        //Filling buffer
        for (int i = (logLength - dataLength); i < bufferSize; i++) {
            buffer[i] = 0;
        }
    }
    return buffer;
}

int TcpServer::getServerPort() const {
    return serverPort;
}

void TcpServer::setLogFileName(std::string newFilename) {
    dataLogName = newFilename;

}
