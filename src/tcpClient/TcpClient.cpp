#include <bits/ios_base.h>
#include "TcpClient.h"

using boost::asio::ip::tcp;


TcpClient::TcpClient() {}

TcpClient::~TcpClient() {}

void TcpClient::mainLoop() {
    int32_t dataRetrieved = 0;
    int32_t fileSize = 0;


    try {

        boost::asio::io_service io_service;
        tcp::resolver resolver(io_service);
        tcp::resolver::query query(serverIp, serverPort);
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        tcp::resolver::iterator end;

        tcp::socket socket(io_service);
        boost::system::error_code error = boost::asio::error::host_not_found;
        while (error && endpoint_iterator != end) {
            socket.close();
            socket.connect(*endpoint_iterator++, error);
        }
        if (error)
            throw boost::system::system_error(error);
        boost::array<char, 20> fileSizeBuffer;


        socket.read_some(boost::asio::buffer(fileSizeBuffer), error);
        // fileSizeBuffer a parser pour obtenir la taille du log sous forme de int (pour vérification de la taille)
        std::string file = "";
        file.append(fileSizeBuffer.data());
        file = file.substr(0, file.find("\n"));

        fileSize = std::stoi(file);

        std::cout << " LogFile size to retrieve : " << fileSize << " bytes \n";

        for (;;) {
            boost::array<char, bufferSize> buf;
            boost::system::error_code error;

            size_t len = socket.read_some(boost::asio::buffer(buf), error);

            if (error == boost::asio::error::eof) {
                break;
            }
            else if (error){
                throw boost::system::system_error(error);

            }

            std::cout.write(buf.data(), len);
            dataRetrieved += len;
        }
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
    std::cout << "Total data retrieved : " << dataRetrieved << " / " << fileSize << " bytes \n";

}

