#ifndef SENSORTEST_TCPCLIENT_H
#define SENSORTEST_TCPCLIENT_H

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

#endif //SENSORTEST_TCPCLIENT_H

class TcpClient {

public:
    TcpClient();

    ~TcpClient();

    void mainLoop();

protected:
    static const int16_t bufferSize = 1500;
    const std::string serverPort = "5000";
    const std::string serverIp = "127.0.0.1";
};