#include "PressureReaderService.h"
#include "iostream"
#include <fstream>
#include "sys/stat.h"
#include <chrono>

using namespace std::chrono;

PressureReaderService::PressureReaderService() {
    sensor = 0;
    logFilename = "pressureLog.txt";
    shouldStop = false;
    started = false;
    sensor = new DummySensor();
};

PressureReaderService::~PressureReaderService() {
    if (sensor != 0) {
        delete sensor;
        sensor = 0;
    }
};

void PressureReaderService::init() {
    if (msSensorReading > 0 && msAveragingInterval > 0 && sensor != 0) {
        sensor->startSensor();
        while(!sensor->hasStarted()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        std::thread * mainThread = new std::thread(&PressureReaderService::mainLoop, this);
        mainThread->detach();
    }
}

void PressureReaderService::mainLoop() {

    checkIfLogFileExists();
    std::ofstream streamToLogFile(logFilename, std::fstream::out | std::fstream::app);
    int16_t averagePressureReading;
    milliseconds averageReadingTimeinMs;
    int16_t loopCount = 0;

    while (!shouldStop) {
        try {
            checkLogSize();
            loopCount++;
            averagePressureReading = averageValuesOverInterval();

            if (streamToLogFile.is_open()) {

                averageReadingTimeinMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
                streamToLogFile << averageReadingTimeinMs.count() << " : " << averagePressureReading << "\n";
            }

            if (loopCount % 10 == 0) {
                streamToLogFile.flush();
            }
            started = true;

        } catch (const std::exception &) {
            perror("Error on PressureReaderService.mainLoop");
            streamToLogFile.close();
        }
    }
    started = false;
    if (streamToLogFile.is_open()) {
        streamToLogFile.close();
    }
}

int16_t PressureReaderService::averageValuesOverInterval() const {
    int16_t averagePressureValue = 0;

    for (int i = 0; i < valuesInInterval; i++) {
                if (int16_t pressureValue = sensor->getPressureValue()) {
                    averagePressureValue += pressureValue / valuesInInterval;
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(msSensorReading));
            }
    return averagePressureValue;
}

void PressureReaderService::checkIfLogFileExists() const {
    if (FILE *file = fopen(logFilename, "r")) {
        fclose(file);
    } else {
        std::fstream stream;
        stream.open(logFilename, std::fstream::in | std::fstream::out | std::fstream::trunc);
        stream << "data_log \n";
        stream.close();
    }
}

void PressureReaderService::checkLogSize() {
    if (fileSize(logFilename) > maxLogFileInBytes) {
        shouldStop = true;
        perror("Max log size reached. Logging stops.");
    }

}

int32_t PressureReaderService::fileSize(const char *path) {
    struct stat results;

    if (stat(path, &results) == 0) {
        return results.st_size;
    } else {
        return -1;
    }
}

void PressureReaderService::stop() {
    shouldStop = true;

}

int16_t PressureReaderService::getInstantPressureValue() {
    return sensor->getPressureValue();
}

void PressureReaderService::setSensor(DummySensor dummySensor) {
    sensor = &dummySensor;
}

void PressureReaderService::setLogFilename(const char * filename) {
    logFilename = filename;
}

const char *PressureReaderService::getLogFilename() {
    return logFilename;
}

bool PressureReaderService::isStarted() const {
    return started;
}
