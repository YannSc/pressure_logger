
#ifndef PRESSURE_LOGGER_PRESSUREREADERSERVICE_H
#define PRESSURE_LOGGER_PRESSUREREADERSERVICE_H

#include "../model/DummySensor.h"

#endif //PRESSURE_LOGGER_PRESSUREREADERSERVICE_H

class PressureReaderService {

public:
    PressureReaderService();
    ~PressureReaderService();

    const char *logFilename;
protected:
    DummySensor *sensor;

    int16_t msSensorReading = 5;
    int16_t msAveragingInterval = 150;
    int32_t maxLogFileInBytes = 100000;
    int16_t valuesInInterval = msAveragingInterval / msSensorReading;
    bool shouldStop;
    bool started;
public:
    bool isStarted() const;

public:
    void init();

    void setSensor(DummySensor dummySensor);

    void setLogFilename(const char * filename);

    void mainLoop();

    void checkLogSize();

    int32_t fileSize(const char *path);

    void stop();

    const char *getLogFilename();

    void checkIfLogFileExists() const;

    int16_t averageValuesOverInterval() const;

    int16_t getInstantPressureValue();

};