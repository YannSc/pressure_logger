#include <fstream>
#include <thread>
#include <cstring>
#include <iostream>
#include "DummySensor.h"
#include "time.h"


DummySensor::DummySensor() {
    filename = "data.txt";
    readingPeriodInMs = 5;
    readingPointer = 0;
    shouldStop = false;
    started = false;
    autoRestart = true;
}

DummySensor::~DummySensor() {

}

const std::string &DummySensor::getFilename() {
    return filename;
}

uint16_t DummySensor::getReadingPeriodInMs() {
    return readingPeriodInMs;
}

uint16_t *DummySensor::getReadingPointer() {
    return readingPointer;
}

void DummySensor::startSensor() {
    shouldStop = false;
    pressureReaderThread = new std::thread(&DummySensor::startPressureReading, this);
    pressureReaderThread->detach();
};

void DummySensor::startPressureReading() {
    std::ifstream pressureDataStream;
    pressureDataStream.open(filename, std::ifstream::in);
    uint16_t pressureReading;
    if (!pressureDataStream) {
        throw std::runtime_error("DummySensor::startPressureReading() : Pressure Data file not found : " + filename);
    } else {
        while (autoRestart && !shouldStop) {
            while (pressureDataStream >> pressureReading) {
                try {
                    readingPointer = &pressureReading;
                    std::this_thread::sleep_for(std::chrono::milliseconds(readingPeriodInMs));
                } catch (std::exception &e) {
                    std::cerr << e.what() << std::endl;
                }
                started = true;

            }
            // Reading from the beginning of the file again
            pressureDataStream.clear();
            pressureDataStream.seekg(0, std::ios::beg);
        }
    }
}

int16_t DummySensor::getPressureValue() const {
    if (readingPointer !=0) {
        return *readingPointer;
    } else {
        return -1; // -1 is considered to be the value when the sensor is off
    }
}

void DummySensor::stopPressureReading() {
    shouldStop = true;
}

void DummySensor::setAutoRestart() {
    autoRestart = true;
}

void DummySensor::setDataFile(std::string readingLogName) {
    filename = readingLogName;
};

void DummySensor::setReadingPeriodInMs(uint16_t newReadingPeriodInMs){
    readingPeriodInMs = newReadingPeriodInMs;
}

bool DummySensor::hasStarted() {
    return started;
};

