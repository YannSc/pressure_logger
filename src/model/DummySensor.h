

#ifndef PRESSURE_LOGGER_DUMMYSENSOR_H
#define PRESSURE_LOGGER_DUMMYSENSOR_H

#include <string>
#include <thread>
#endif //PRESSURE_LOGGER_DUMMYSENSOR_H


class DummySensor {

public:
    DummySensor();
    ~DummySensor();

protected:
    std::string     filename;
    uint16_t *      readingPointer;
    std::thread *   pressureReaderThread;
    bool            shouldStop;
    bool            started;
    bool            autoRestart;


public:
    int16_t getPressureValue() const;
    uint16_t         readingPeriodInMs;
    uint16_t getReadingPeriodInMs();
    uint16_t * getReadingPointer();
    void startSensor();
    void startPressureReading();
    void stopPressureReading();
    void setAutoRestart();
    const std::string &getFilename();
    void setReadingPeriodInMs(uint16_t readingPeriodInMs);

    void setDataFile(std::string readingLogName);

    bool hasStarted();
};
