#include <iostream>
#include "src/service/PressureReaderService.h"
#include "src/tcpServer/TcpServer.h"

int main() {

    while (1) {
        try {

            DummySensor sensor = DummySensor();
            std::cout << "Pressure Sensor created... \n";

            PressureReaderService readerService = PressureReaderService();
            readerService.setSensor(sensor);
            readerService.init();
            std::cout << "Pressure Logger started... \n";
            while (readerService.isStarted()) {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
            TcpServer tcpServer = TcpServer();
            std::cout << "TCP Server starting on port : " << tcpServer.getServerPort() << " \n";

            tcpServer.mainLoop();
        } catch (const std::exception &e) {
            perror("Error on runtime, tcpServer restarts in 3 seconds...");
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));

        }

    }
}