cmake_minimum_required(VERSION 3.5)
project(pressure_logger)
include_directories(/usr/include/x86_64-linux-gnu/)
set(CMAKE_CXX_STANDARD 11)

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package(Boost 1.36.0 COMPONENTS system)


add_executable(pressure_logger
        main.cpp
        src/model/DummySensor.h
        src/model/DummySensor.cpp
        src/service/PressureReaderService.h
        src/service/PressureReaderService.cpp
        src/tcpServer/TcpServer.cpp
        src/tcpServer/TcpServer.h
        src/tcpClient/TcpClient.h
        src/tcpClient/TcpClient.cpp)

configure_file(data.txt ${CMAKE_CURRENT_BINARY_DIR}/data.txt COPYONLY)
target_link_libraries(pressure_logger pthread ${Boost_LIBRARIES})