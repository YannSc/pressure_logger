#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "../src/model/DummySensor.h"

void waitNextReading(DummySensor *sensor) {
    int16_t reading = sensor->getPressureValue();
    while (reading == sensor->getPressureValue()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
    }
}


TEST_CASE("should_init_sensor", "[dummySensor]") {

    DummySensor sensor = DummySensor();
    sensor.setDataFile("test_data.txt");
    REQUIRE(sensor.getFilename() == "test_data.txt");
    REQUIRE(sensor.getReadingPeriodInMs() == 5);
}

TEST_CASE("should_get_reading", "[dummySensorReading]") {
    DummySensor sensor = DummySensor();
    sensor.setDataFile("test_data.txt");

    uint16_t readingPeriod = 5;
    sensor.setReadingPeriodInMs(readingPeriod);
    sensor.startSensor();

    uint16_t pressureReading;

    waitNextReading(&sensor);
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 0);

    waitNextReading(&sensor);
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 100);

    waitNextReading(&sensor);
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 1000);

    waitNextReading(&sensor);
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 65535);

    waitNextReading(&sensor);
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 20);

    sensor.stopPressureReading();
    std::this_thread::sleep_for(std::chrono::milliseconds(sensor.getReadingPeriodInMs()));
    pressureReading = sensor.getPressureValue();
    REQUIRE(pressureReading == 0);

}