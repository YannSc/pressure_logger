#include <fstream>
#include "catch.hpp"
#include "../src/service/PressureReaderService.h"
#include "../src/tcpServer/TcpServer.h"

TEST_CASE( "should open log and get value", "[tcpserver]" ) {

 TcpServer tcpServer =  TcpServer();
 std::string testLogName = "test_pressureLog.txt";
 tcpServer.setLogFileName(testLogName);

 std::ifstream  dataStream;
 dataStream = tcpServer.readerInit();
 int fileSize = tcpServer.getLogSize();
 REQUIRE(fileSize == 630);

 std::string dataFromServer = tcpServer.readerGetValue(&dataStream, 1500);
 REQUIRE(dataFromServer == "1539118647329 : 4486\n1539118647483 : 5213\n1539118647637 : 5424\n1539118647791 : 5851\n1539118647945 : 4794\n1539118648100 : 5469\n1539118648254 : 4956\n1539118648409 : 5055\n1539118648563 : 5164\n1539118648718 : 4934\n1539118648872 : 5171\n1539118649026 : 4905\n1539118649181 : 5272\n1539118649336 : 4795\n1539118649490 : 5605\n1539118649644 : 4609\n1539118649797 : 4938\n1539118649952 : 5394\n1539118650107 : 4923\n1539118650262 : 5334\n1539118650417 : 4117\n1539118650570 : 5814\n1539118650726 : 5864\n1539118650880 : 4882\n1539118651035 : 5207\n1539118657904 : 5328\n1539118658059 : 5248\n1539118658213 : 4744\n1539118658368 : 4321\n1539118658523 : 4372\n");

}
