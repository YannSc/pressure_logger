#include <fstream>
#include "catch.hpp"
#include "../src/service/PressureReaderService.h"


TEST_CASE("should init service and start logging", "[pressureReader]") {

    PressureReaderService pressureReaderService =  PressureReaderService();
    pressureReaderService.init();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::ifstream loggerFile(pressureReaderService.getLogFilename());
    REQUIRE(loggerFile.good());

}

TEST_CASE("should average values", "[pressureReaderAverage]") {

    PressureReaderService pressureReaderService =  PressureReaderService();
    pressureReaderService.init();


    // To synchronize readings (fluctuates otherwise)
    while (pressureReaderService.getInstantPressureValue() != 9995) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
    }
    int16_t averagePressureReading = pressureReaderService.averageValuesOverInterval();
    // Average value over 150ms
    REQUIRE(averagePressureReading==5154);

}
